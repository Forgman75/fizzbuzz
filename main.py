# -*- coding: utf-8 -*-

"""This program generates and returns a list with numbers
 from 1 to max inclusive. If the number is divided by 3 the program puts a Fizz
 word into the list, and if the number is divided by 5 the program puts a Buzz
 word into the list. If the number is divided both by 3 and 5 the program puts
 FizzBuzz into the list."""


def get_fizzbuzz_list(max):
  max=max+1
  list = []
  for i in range(1,max):
    if i%3 == 0 and i%5 == 0:
        list.append("FizzBuzz")
    elif i%3 == 0:
        list.append("Fizz")
    elif i%5 == 0:
        list.append("Buzz")
    else:
        list.append(i)
  return list


number = int(input("Введите верхнюю границу диапазона чисел:"))
print(get_fizzbuzz_list(number))
        
